package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Map;


@XmlRootElement
public class Region {

    private Integer id;
    private Integer parentId;
    private Long extractionTS;
    private Integer language;
    private Integer type;
    private String name;
    private Integer projectId;
    private String commitId;
    private Long commitTs;

    private List<MetricStore> metricStoreList;

    public List<MetricStore> getMetricStoreList() {
		return metricStoreList;
	}

	public void setMetricStoreList(List<MetricStore> metricStoreList) {
		this.metricStoreList = metricStoreList;
	}

	public Region(Map<String, String> map) {
        this.setId(map.get("id"));
        this.setParentId(map.get("parentid"));
        this.setExtractionTS(map.get("extractionts"));
        this.setLanguage(map.get("language"));
        this.setType(map.get("type"));
        this.setName(map.get("name"));
        this.setProjectId(map.get("projectid"));
        this.setCommitId(map.get("commitid"));
        this.setCommitTs(map.get("committs"));

        System.out.println(this);
    }

    public Region() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = (id == null) ? null : Integer.parseInt(id);
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = (parentId == null) ? null : Integer.parseInt(parentId);
    }

    public Long getExtractionTS() {
        return extractionTS;
    }

    public void setExtractionTS(Long extractionTS) {
        this.extractionTS = extractionTS;
    }

    public void setExtractionTS(String extractionTS) {
        this.extractionTS = (extractionTS == null) ? null : Long.parseLong(extractionTS);
    }

    public Integer getLanguage() {
        return language;
    }

    public void setLanguage(Integer language) {
        this.language = language;
    }

    public void setLanguage(String language) {
        this.language = (language == null) ? null : Integer.parseInt(language);
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = (type == null) ? null : Integer.parseInt(type);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = (projectId == null) ? null : Integer.parseInt(projectId);
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    public Long getCommitTs() {
        return commitTs;
    }

    public void setCommitTs(Long commitTs) {
        this.commitTs = commitTs;
    }

    public void setCommitTs(String commitTs) {
        this.commitTs = (commitTs == null) ? null : Long.parseLong(commitTs);
    }

    @Override
    public String toString() {
        return "Region [id=" + id + ", parentId=" + parentId + ", extractionTS=" + extractionTS + ", language="
                + language + ", type=" + type + ", name=" + name + ", projectId=" + projectId + ", commitId=" + commitId
                + ", commitTs=" + commitTs + "]";
    }
}
