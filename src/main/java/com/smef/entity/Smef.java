package com.smef.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Smef {
    public Smef(Project project, List<Region> regions) {
        this.setProject(project);
        this.setRegions(regions);
    }

    public Smef() {
    }

    private Project project = new Project();

    private List<Region> regions = new ArrayList<>();


    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

}
