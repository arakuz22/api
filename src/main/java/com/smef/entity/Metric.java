package com.smef.entity;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement//https://youtu.be/stW0gyeMgDI?t=8m20s
public class Metric {
	
	private int id;
	private String shortName;
	private String longName;
	private String descriptions;
	private String metcricspediaReference;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	public String getLongName() {
		return longName;
	}
	public void setLongName(String longName) {
		this.longName = longName;
	}
	public String getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	public String getMetcricspediaReference() {
		return metcricspediaReference;
	}
	public void setMetcricspediaReference(String metcricspediaReference) {
		this.metcricspediaReference = metcricspediaReference;
	}
	@Override
	public String toString() {
		return "Metric [id=" + id + ", shortName=" + shortName + ", longName=" + longName + ", descriptions="
				+ descriptions + ", metcricspediaReference=" + metcricspediaReference + "]";
	}
	
	
}