package com.smef.api;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.smef.db.SmefDB;
import com.smef.entity.*;
import com.smef.utils.HttpQueryParser;
import com.smef.utils.SmefJAXB;
import com.smef.utils.XMLValidator;

import com.smef.exceptions.*;

@Path("smef")
public class ApiSMEF {

    SmefDB db = new SmefDB();

    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Smef> getSmefs() {

        return getSmefs("");
    }

    @GET
    @Path("{query}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Smef> getSmefs(@PathParam("query") String HttpQuery) {

        System.out.println(new File("").getAbsolutePath());
        List<Map<String, String>> paramsList = HttpQueryParser.parseQuery(HttpQuery, "&", ",");

        List<Project> projects = new ArrayList<>();
        for (Map<String, String> map : paramsList) {
            projects.add(new Project(map));
        }

        return db.getSmefsByProjects(projects);
    }

    @POST
    @Consumes({MediaType.APPLICATION_XML})
    public Response postSmef(File smef_xml) {

        if (!XMLValidator.validateSmefXML(smef_xml))
            throw new InvalidInputException("XML Validation error");

        Smef smef = SmefJAXB.unmarshall(smef_xml);

        db.postSmef(smef);

        return Response.status(Response.Status.CREATED)
                .entity("Item was successfully created!")
                .build();
    }

}
