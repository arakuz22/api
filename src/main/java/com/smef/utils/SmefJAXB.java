package com.smef.utils;

import com.smef.entity.Smef;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class SmefJAXB {

    public static Smef unmarshall(File xml_smef) {
        Smef smef = new Smef();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Smef.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            smef = (Smef) unmarshaller.unmarshal(xml_smef);
        } catch (JAXBException e) {
            System.out.println("Smef JAXB Exception (unmarshalling): " + e.getMessage());
        }
        return smef;
    }

    public static File marshall(Smef smef) {
        File smef_xml = new File("webapps/api/WEB-INF/classes/com/smef/data/smef_temp");
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(Smef.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(smef, smef_xml);
        } catch (JAXBException e) {
            System.out.println("Smef JAXB Exception (marshalling): " + e.getMessage());
        }
        return smef_xml;
    }
}
